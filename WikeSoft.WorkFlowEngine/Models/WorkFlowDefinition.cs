﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class WorkFlowDefinition
    {
        ///<summary>
        /// 流程定义Id
        ///</summary>
        public string Id { get; set; } // FlowDefId (Primary key) (length: 50)

        ///<summary>
        /// 流程定义Key值，必填
        ///</summary>
        [Display(Name = "定义Key")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string FlowDefKey { get; set; } // FlowDefKey (length: 100)

        ///<summary>
        /// 流程定义名称
        ///</summary>
        [Display(Name = "流程定义名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string FlowDefName { get; set; } // FlowDefName (length: 100)
        /// <summary>
        /// 类别ID
        /// </summary>
        public string CategoryId { get; set; }

        [Display(Name = "所属分类")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string CategoryName { get; set; }

       

        [Display(Name = "业务路径")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string BusinessUrl { get; set; }

        [Display(Name = "审核地址")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string AuditUrl { get; set; }

    }
}
