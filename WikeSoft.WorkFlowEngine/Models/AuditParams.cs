﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Enum;

namespace WikeSoft.WorkFlowEngine.Models
{

    public class AuditParams
    {
        public string NodeRecordId { get; set; }

        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserMsg { get; set; }
        public string Condition { get; set; }
        public string TargetUserId { get; set; }

        public TaskAudit TaskAudit { get; set; }
         
    }
}
