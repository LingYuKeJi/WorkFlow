﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Web.Mvc;
using WikeSoft.WorkFlowEngine;
using WikeSoft.WorkFlowEngine.Enum;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Interfaces;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.Web.Controllers.WorkFlow
{
    public class FlowDesignController : BaseController
    {

     
        private readonly IWorkFlowDesignService _flowDesignService;
        public FlowDesignController(IWorkFlowDesignService flowDesignService)
        {
            _flowDesignService = flowDesignService;
        }


        public ActionResult GetList(FlowDefFilter filter)
        {
            PagedResult<WorkFlowModel> defs=  _flowDesignService.GetList( filter);

            return JsonOk(defs);
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(WorkFlowDefinition model)
        {
            if (ModelState.IsValid)
            {
                _flowDesignService.AddFlowDef(model);
                return RedirectToAction("Index");
            }
           
            return View();
        }

       
        public ActionResult Edit(String id)
        {
            WorkFlowDefinition model = _flowDesignService.GetFlowDef(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(WorkFlowDefinition model)
        {
            if (ModelState.IsValid)
            {
                bool flag = CeckHolidayApply(model.Id);
                if (flag)
                {
                    ViewBag.IsHoliday = true;
                    return View(model);
                }
                var flowMessage = _flowDesignService.UpdateFlowDef(model);
                if (flowMessage.Code == CodeEum.Success)
                    return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult FlowDesign()
        {
            return View();
        }
         

        public ActionResult Delete(IList<string> ids)
        {
            
            FlowMessage message = _flowDesignService.DeleteFlowDef(ids);
            if (message.Code == CodeEum.Success)
            {
                return Ok();
            }
            return Fail(message.Message);
        }


       

        public ActionResult FlowDiagram(string id)
        {
            WorkFlowModel flow = _flowDesignService.GetFlowModel(id);
            return Json(flow, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDiagram(WorkFlowModel flowDef)
        {
             bool flag = CeckHolidayApply(flowDef.Id);
            //flag = false;
            if (flag)
            {
                FlowMessage messageInstanse1 = new FlowMessage();
                messageInstanse1.Code = CodeEum.Fail;
                messageInstanse1.Message = "演示用例不能修改";
                return Json(messageInstanse1, JsonRequestBehavior.AllowGet);
            }
            FlowMessage messageInstanse = _flowDesignService.SaveDiagram(flowDef);
            return Json(messageInstanse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveDiagramNew(WorkFlowModel flowDef)
        {
            bool flag = CeckHolidayApply(flowDef.Id);
            //flag = false;
            if (flag)
            {
                FlowMessage messageInstanse1 = new FlowMessage();
                messageInstanse1.Code = CodeEum.Fail;
                messageInstanse1.Message = "演示用例不能修改";
                return Json(messageInstanse1, JsonRequestBehavior.AllowGet);
            }
            FlowMessage messageInstanse = _flowDesignService.SaveDiagramWithNewVersion(flowDef);
            return Json(messageInstanse, JsonRequestBehavior.AllowGet);
        }

        private bool CeckHolidayApply(string defId)
        {

            if (defId.Equals("96F5B45A-6C94-4058-A76C-879FE6FFC09B"))
            {
                return false;
            }
            return false;
        }

        public void Pic(string id)
        {
            var image = _flowDesignService.GetBitmap(id);
            MemoryStream stream = new MemoryStream();
            image.Save(stream, ImageFormat.Jpeg);
            HttpContext.Response.Clear();
            HttpContext.Response.ContentType = "image/jpeg";
            HttpContext.Response.BinaryWrite(stream.ToArray());

        }
    }
}