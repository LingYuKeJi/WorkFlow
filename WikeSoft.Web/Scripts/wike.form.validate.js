﻿/*
** 此js是添加，编辑页面调用
*/
 

$(function() {
    
    $("#OutRemark").change(function () {
        setOutRemakrValidate();
    });

    $("#Bidding").change(function () {
        setBiddingValidate();
    });

    setFormValidate();
});


function setOutRemakrValidate() {
    if ($("#OutMoney").length > 0) {
        var outRemark = $("#OutRemark").val();
        if (outRemark == "ca2a81fe-6d55-453b-b59c-a1b1b3aac281") { //出场
            $("#OutMoney").removeAttr("readonly");
            $("#OutMoney").rules("add", { required: true, messages: { required: "出场费不能为空" } });
        } else { //不出场
            $("#OutMoney").rules("remove", "required");
            $("#OutMoney").attr("readonly", "readonly");
            $("#OutMoney").val("");
            var form = $("#userForm");
            if (form != null) {
                form.valid();
            }

        }
    }
    
}

function setBiddingValidate() {
    if ($("#Bidding").length > 0) {
        var bidding = $("#Bidding").val();
        if (bidding == "ef8f0ec6-309c-4889-b1dc-94cf139d370f") { //投标
            $("#BiddingMoney").removeAttr("readonly");
            $("#BiddingMoney").rules("add", { required: true, messages: { required: "中标补贴不能为空" } });
        } else {
            $("#BiddingMoney").rules("remove", "required");
            $("#BiddingMoney").attr("readonly", "readonly");
            $("#BiddingMoney").val("");
            var form = $("#userForm");
            if (form != null) {
                form.valid();
            }
        }
    }
   
}

function setFormValidate() {

    setOutRemakrValidate();
    setBiddingValidate();

}