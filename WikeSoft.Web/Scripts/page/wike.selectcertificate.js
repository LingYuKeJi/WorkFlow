﻿function openDialog(dom) {
    top.layer.open({
        title: '证书选择',
        type: 2,
        content: "/CertificateType/SelectData",
        area: ['1000px', '600px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var data = $(layero).find("iframe")[0].contentWindow.getContent();
            setData(data, dom);
            if (data != null) {
                top.layer.close(index);
            }
          
        }, cancel: function () {
            return true;
        }
    });
}

function delRow(dom) {
    $(dom).parent().parent().remove();
}
function setData(data, dom) {
    if (data != null) {
        $(dom).parent().parent().parent().parent().find("tr").each(function (i) {
            if (i != 0) {
                $(this).remove();
            }
        });
        var html = "<tr><td><input type='hidden' name='CertificateId' value='" + data.CertificateId + "' /><input type='hidden' name='CertificateName' value='" + data.CertificateName + "' /><input type='hidden' name='RegisterState' value='" + data.RegisterState + "' /><input type='hidden' name='ACertificateState' value='" + data.ACertificateState + "' /><input type='hidden' name='BCertificateState' value='" + data.BCertificateState + "' /><input type='hidden' name='CCertificateState' value='" + data.CCertificateState + "' />" + data.CertificateName + "</td><td>" + data.RegisterStateStr + "</td><td>" + data.ACertificateStateStr + "</td><td>" + data.BCertificateStateStr + "</td><td>" + data.CCertificateStateStr + "</td><td><a href='#' onclick='delRow(this);'>删除</a></td></tr>";
        $(dom).parent().parent().parent().parent().append(html);

        //当证书改变过后，清空子项的值。
        $("#subTable").find("tr").each(function (i) {
            if (i != 0) {
                $(this).remove();
            }
        });
       
    }
   
}

function openMajorDialog(dom) {
    var id = $("#mainTable input[name='CertificateId']").val();
    if (typeof (id) == "undefined") {
        alert("请先设置主项");
        return;
    }
    top.layer.open({
        title: '功能查询',
        type: 2,
        content: "/CertificateType/SelectMajorView?id="+id,
        area: ['1000px', '600px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var data = $(layero).find("iframe")[0].contentWindow.getContent();
            setMajorData(data, dom);
            if (data != null) {
                top.layer.close(index);
            }
        }, cancel: function () {
            return true;
        }
    });
    
}

function setMajorData(data, dom) {
    if (data != null) {
        var html = "<tr><td><input type='hidden' name='CertificateId' value='" + data.CertificateId + "' /><input type='hidden' name='CertificateName' value='" + data.CertificateName + "' /><input type='hidden' name='RegisterState' value='" + data.RegisterState + "' />" + data.CertificateName + "</td><td>" + data.RegisterStateStr + "</td><td><a href='#' onclick='delRow(this);'>删除</a></td></tr>";
        $(dom).parent().parent().parent().parent().append(html);
       
    }
   
   
}


