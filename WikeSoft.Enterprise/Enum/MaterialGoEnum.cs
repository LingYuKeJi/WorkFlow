﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 原件去向
    /// </summary>
    public enum MaterialGoEnum
    {
        /// <summary>
        /// 库存
        /// </summary>
        [Description("库存")]
        Store = 0,

        /// <summary>
        /// 人才部借出
        /// </summary>
        [Description("人才部借出")]
        GetByCustomerDepartment =1,

        /// <summary>
        /// 企业部借出
        /// </summary>
        [Description("企业部出证")]
        GetByCompanyDepartment = 2
    }
}
