﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{

    public enum CCertificateTypeEnum
    {

        /// <summary>
        /// 带C
        /// </summary>
        [Description("带C")]
        HaveC = 0,

        /// <summary>
        /// 考C
        /// </summary>
        [Description("考C")]
        ExamC = 1,

        /// <summary>
        /// 不考C
        /// </summary>
        [Description("不考C")]
        NoExamC = 2,
        /// <summary>
        /// 已换
        /// </summary>
        [Description("已换C证")]
        Changed = 3,

        /// <summary>
        /// 已转
        /// </summary>
        [Description("已转C证")]
        Turn = 4,
    }
}
