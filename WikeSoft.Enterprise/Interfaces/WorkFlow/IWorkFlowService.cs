﻿using System;
using System.Collections.Generic;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.Enterprise.Interfaces.WorkFlow
{
    public interface IWorkFlowService
    {
        string GetAuthorityUser(WorkFlowAuthority authority, string userId);

        string GetAuthorityUsers(List<WorkFlowAuthority> authorities, string userId );

       

    }
}
